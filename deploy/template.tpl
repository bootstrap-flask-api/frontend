#!/bin/bash
yum update -y
amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2
yum install -y httpd mariadb-server git
git clone https://gitlab.com/bootstrap-flask-api/frontend.git
mv frontend/www/* var/www/
systemctl start httpd
systemctl enable httpd