output "website_url" {
  value = "URL access: http://${aws_instance.web.public_ip}"
}