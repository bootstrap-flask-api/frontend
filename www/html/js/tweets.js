function display_tweets(tweets){
    tweets.forEach(tweet => {
        create_tweet_card(tweet["id"], tweet["text"])
    });
}

function create_tweet_card(id, message){
    let date = new Date().toLocaleString();
    let new_tweet = document.createElement("div");
    new_tweet.setAttribute("id", `tweet-id-${id}`);
    let card = `
        <div class="row d-flex justify-content-center">
            <div class="card border-primary mb-3" style="width: 540px;">
                <div class="row no-gutters">
                    <div class="col-md-4 text-center align-self-center">
                        <img src="img/tweet_logo.png" class="card-img" id="tweet-logo" alt="A magnificient and really fat blue bird">
                    </div>
                    <div class="col-md-8">
                        <div class="row no-gutters justify-content-end">
                            <div class="tweet-icons" id="edit-tweet" style="color:#0275d8 ;" onclick="init_update_tweet()">
                                <i class="far fa-edit"></i>
                            </div>
                            <div class="tweet-icons" id="delete-tweet" style="color:#E74C3C;" onclick="init_call('delete')">
                                <i class="far fa-trash-alt"></i>
                            </div>
                        </div>
                        <div class="row">
                            <div class="card-body">
                                <p class="card-text" id="tweet-${id}-text">${message}</p>
                                <p class="card-text" id="tweet-${id}-date"><small class="text-muted">Tweeted the ${date}</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
    new_tweet.innerHTML = card;
    document.getElementById("container-tweets-view").prepend(new_tweet);
}

function init_update_tweet(){
    id = event.currentTarget.closest("#container-tweets-view > div").getAttribute('id').substr(9);
    init_message = document.getElementById(`tweet-${id}-text`).innerHTML;
    $('#exampleModal').modal('show');
    let tweet_text_to_update = `<textarea class="form-control" id="update-tweet-text" value=${id} rows=2 aria-label="update a tweet">${init_message}</textarea>`;
    document.getElementById("tweet-to-update").innerHTML = tweet_text_to_update;
}

function updated_tweet(id, new_message){
    $('#exampleModal').modal('hide');
    document.getElementById(`tweet-${id}-text`).innerHTML = new_message;
}

function delete_tweet(id){
    document.getElementById(`tweet-id-${id}`).remove();
}

function display_error(){
    alert("Ouuuups, something went wrong")
}

function api_call(method, id, message){
    backend_ip == null ? backend_ip = document.getElementById('backend-ip').value : backend_ip = backend_ip;
    let url = `http://${backend_ip}/v1/tweets` + (id != null ? `/${id}` : "");
    try{
        fetch(url, 
            {method: method,
            mode: 'cors',
            cache: 'no-cache',
            headers: {'content-type': 'application/json'},
            redirect: 'follow',
            referrer: 'no-referrer',
            body: message != null ? `{"text": "${message}"}` : null
        }).then(response => {
            if(response.statusText != "NO CONTENT" && response.status.toString()[0] === "2"){
                response.json().then(data => {
                    if(method === "GET"){
                        display_tweets(data);
                    } else if(method === 'POST'){
                        create_tweet_card(data["id"], data["text"]);
                        document.getElementById("message-post").value = "";
                    } else if(method === 'PUT'){
                        updated_tweet(data["id"], data["text"]);
                    }
                });
            }else{
                if(response.status === 204){
                    delete_tweet(id);
                }else{
                    display_error();
                }
            }
        });
    }catch(error){
        alert(error.message);
    }
}

function init_call(method){
    let id;
    let message;
    switch (method) {
        case 'get':
            api_call('GET', null, null);
            break;
        case 'getid':
            value = document.getElementById("id-get").value;
            id = value != "" ? value : null;
            api_call('GET', id, null);
            break;
        case 'post':
            message = document.getElementById("message-post").value;
            api_call('POST', null, message);
            break;
        case 'put':           
            id = document.getElementById('update-tweet-text').getAttribute('value');
            message = document.getElementById('update-tweet-text').value;
            api_call('PUT', id, message);
            break;
        case 'delete':
            id = event.currentTarget.closest("#container-tweets-view > div").getAttribute('id').substr(9);
            api_call('DELETE', id, null);
            break;
        default:
            api_call('GET', null, null);
    }
}

function init_tweets(){
    init_call('get');
}
