document.addEventListener("DOMContentLoaded",function(){
    if(backend_ip != null){
        init_app(backend_ip);
    }else{
        document.getElementById("server-not-detected").classList.remove('hidden');
    }
});

function manual_init(){
    backend_ip = document.getElementById("server-not-detected-ip").value;
    let url = `http://${backend_ip}/documentation`;
    fetch(url).then(response => {
        document.getElementById("server-not-detected-text").innerText = "Server used:";
        document.getElementById("server-not-detected-text").style.color = "green";
        document.getElementById("server-not-detected-button").className = "btn btn-success specific-width"
        document.getElementById("server-not-detected-button").innerHTML = "CHANGE"
        init_app(backend_ip);
    }).catch(err => {
        document.getElementById("server-not-detected-text").innerText = "Backend not found. Please set the server IP:PORT";
        document.getElementById("server-not-detected-text").style.color = "red";
        document.getElementById("server-not-detected-button").className = "btn btn-danger specific-width"
    });
}

function init_app(backend_ip){
    let url = `http://${backend_ip}/v1`;
    fetch(`${url}/tweets`, {method: 'GET'}).then(response => {
        if(response.status === 200){
            document.getElementById("tweets-container").classList.remove('hidden');
            init_tweets();
        }
    }).catch(err => {
        console.log("no tweets API detected")
    });
    fetch(`${url}/s3`).then(response => {
        if(response.status === 200){
            document.getElementById("objects-container").classList.remove('hidden');
            // add init s3
        }
    }).catch(err => {
        console.log("no s3 API detected")
    });
    fetch(`${url}/sqs`).then(response => {
        if(response.status === 200){
            document.getElementById("topics-container").classList.remove('hidden');
            // add init sqs
        }
    }).catch(err => {
        console.log("no sqs API detected")
    });
}

let backend_ip;
