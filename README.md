# Flask API frontend

## What it does
- very basic website to call the backend


## How to run
- clone the repo anywhere
- from the www dir, run:
```
$ python -m http.server 80
```
- in your browser, connect to [localhost](http://localhost)
- enter the IP:PORT of the backend in the first field (backend ip will be automatically 
  set if deployed via [this repo](https://gitlab.com/bootstrap-flask-api/app-deploy))

## How to deploy on AWS (optional)

- [install terraform](https://learn.hashicorp.com/terraform/getting-started/install)
- open your terminal in frontend/deploy/ then:
```
$ terraform init
$ terraform apply
```
- to stop being asked for a region and profile, uncomment `default` in 00_variables.tf
and set your AWS region and profile name:
```
variable "aws_region" {
  type        = string
  description = "AWS region name to deploy the infrastructure"
  #default     = ""     # the aws region that will host the infrastructure
}

variable "aws_profile" {
  type        = string
  description = "AWS profile that will be used to deploy the infrastructure"
  #default     = ""     # the IAM user that will perform the deployment
}
```
- if you want to access the instance, replace in the key_name variable the `null` in 
default by a ssh key registered in your AWS account
```
variable "key_name" {
  type        = string
  description = "Key stored in AWS for ssh access"
  default     = null # change for your ssh key to access the instance, if needed
}
```
- the url will be displayed when the build finishes. It takes up to 1 minute to be actually
available (EC2 instance initialisation)
- clean after your tests
```
$ terraform destroy
```
